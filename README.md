# Segment, measure and plot bright field images of cells in suspension

## Analysis

### Goal

The goal of this project is estimate the diameter of different types of cells based on bright-field images. For this, cells needs to be segmented, measured and filtered. Measurements are plotted to show their mean diameter.

### Pipeline

Image are detected using Cellpose cyto2 pre-trained model. A few parameters are offered to the users to fine-tune the segmentation. Area, perimeter, eccentricity, circularity and diameter are then calculated and optionally used to filter undesired detected cells. Images are processed in batches according to their folder, to offer the possibility to either process all images under the same conditions or to allow flexibility when different cell types need different settings. Cellpose segmentation images are exported for easy inspection and an additional overlay with the desired cell-by-cell property is saved to disk to make it easier to filter based on visual clues.

These filtered measurementes are then exported and used to create a few visualizations.

### Instructions

If you want to run this analysis yourself follow these instructions.

1. [Install conda](https://docs.conda.io/en/latest/miniconda.html) on your computer
2. Use conda to create a virtual environment using the provided file:

```bash
$ conda env create -f env.yml
```

3. Activate the environment

```bash
$ conda activate cellpose-plot
```

4. Start up Jupyter lab and run the analysis

```bash
$ jupyter lab
```

## Content

- data: please place your images in independent folders here. For the final figures we will extract the date and cell type from the folder names. So make sure to name all the folders with the following pattern: `celltype date`. The date and cell type don't need to have a specific format, they just to be separated by a space. Example: `htc116 01.03.2023`
- scripts: Jupyter notebook to run the analysis
- output: contains exported measurements and corresponding figures generated with the scripts from all original images used for the project.

All the original images are not provided, you can find an example of input image and output segmentation in the corresponding folders. Please place in the data folder additional data you want to analyze.

## Author

Marco Dalla Vecchia dallavem@igbmc.fr
